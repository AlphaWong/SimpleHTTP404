package middleware

import (
	"context"
	"log"
	"net/http"
	"net/http/httputil"

	"gitlab.com/SimpleHTTP404/utils"

	uuid "github.com/satori/go.uuid"
)

type Middleware struct {
	next http.Handler
}

func LoggingMiddleware(next http.Handler) *Middleware {
	return &Middleware{next: next}
}

func (m *Middleware) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	footPrint := uuid.NewV4().String()
	dump, _ := httputil.DumpRequest(r, true)
	log.Printf("footPrint: %s request: %v", footPrint, string(dump))
	ctx := context.WithValue(r.Context(), utils.FootPrintKey, footPrint)
	m.next.ServeHTTP(w, r.WithContext(ctx))
}
