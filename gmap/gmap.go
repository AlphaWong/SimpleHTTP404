package gmap

import (
	geo "github.com/Alphawong/google-geolocate"
	"gitlab.com/SimpleHTTP404/types"
)

var MapService MapServiceInterface

type MapServiceInterface interface {
	Get(string, string) (*types.GeoCodeResponse, error)
}

func AddMapService(k string) {
	MapService = NewGoogleMapService(k)
}

func NewGoogleMapService(k string) MapServiceInterface {
	return &GoogleMapService{
		GMap: geo.NewGMapInstance(geo.WithGMapKey(k)),
	}
}

type GoogleMapService struct {
	GMap *geo.GMap
}

func (g *GoogleMapService) Get(address, region string) (*types.GeoCodeResponse, error) {
	ll, err := g.GMap.GetGeoCode(address, region)
	if nil != err {
		return nil, err
	}
	return &types.GeoCodeResponse{
		Lat: ll.Lat,
		Lng: ll.Lng,
	}, nil
}
