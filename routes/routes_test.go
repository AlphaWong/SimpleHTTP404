package routes

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/SimpleHTTP404/gmap"
	"gitlab.com/SimpleHTTP404/types"
	"gitlab.com/SimpleHTTP404/utils"

	"github.com/julienschmidt/httprouter"
	"github.com/stretchr/testify/assert"
)

func TestIsValidGeoResponse(t *testing.T) {
	type args struct {
		gcr *types.GeoCodeResponse
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			"Test pass IsValidGeoRespone",
			args{
				&types.GeoCodeResponse{
					FootPrint: "I am foot print",
					Lng:       "123",
					Lat:       "321",
				},
			},
			false,
		},
		{
			"Test fail IsValidGeoRespone",
			args{
				&types.GeoCodeResponse{
					"",
					"",
					"",
				},
			},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := IsValidGeoResponse(tt.args.gcr); (err != nil) != tt.wantErr {
				t.Errorf("IsValidGeoResponse() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

type MockSuccessMapService struct{}

func (g *MockSuccessMapService) Get(address, region string) (*types.GeoCodeResponse, error) {
	return &types.GeoCodeResponse{
		Lat: "123",
		Lng: "321",
	}, nil
}

func TestPassGeoCodeHandler(t *testing.T) {
	gmap.MapService = &MockSuccessMapService{}
	jsonString := `
		{
			"address": "HKU",
			"region": "HK"
		}
	`
	req := httptest.NewRequest("POST", "http://127.0.0.1:3000/GeoCode", strings.NewReader(jsonString))
	ctx := context.WithValue(req.Context(), utils.FootPrintKey, "")
	req = req.WithContext(ctx)
	resp := httptest.NewRecorder()
	GeoCodeHandler(resp, req, httprouter.Params{})

	var tmpMap map[string]string
	json.NewDecoder(resp.Result().Body).Decode(&tmpMap)
	delete(tmpMap, "footPrint")
	defer resp.Result().Body.Close()
	assert.Equal(t, map[string]string{
		"lat": "123",
		"lng": "321",
	}, tmpMap, "It should be equal")
}

func TestFailGeoCodeHandlerByInvalidFieldValue(t *testing.T) {
	gmap.MapService = &MockSuccessMapService{}
	jsonString := `
		{
			"address": 123.321,
			"region": true
		}
	`
	req := httptest.NewRequest("POST", "http://127.0.0.1:3000/GeoCode", strings.NewReader(jsonString))
	ctx := context.WithValue(req.Context(), utils.FootPrintKey, "")
	req = req.WithContext(ctx)
	resp := httptest.NewRecorder()
	GeoCodeHandler(resp, req, httprouter.Params{})
	defer resp.Result().Body.Close()

	assert.Equal(t, resp.Result().StatusCode, http.StatusBadRequest, "It should be equal")

}

type MockAddressNotFoundMapService struct{}

func (g *MockAddressNotFoundMapService) Get(address, region string) (*types.GeoCodeResponse, error) {
	return &types.GeoCodeResponse{
		Lat: "",
		Lng: "",
	}, nil
}

func TestFailGeoCodeHandlerByInvaildAddress(t *testing.T) {
	gmap.MapService = &MockAddressNotFoundMapService{}
	jsonString := `
		{
			"address": "!@#$%$^&*^&*(!@#!@#)@$%*#$%*#$500123!$!@$!@#!@3",
			"region": "SUN"
		}
	`
	req := httptest.NewRequest("POST", "http://127.0.0.1:3000/GeoCode", strings.NewReader(jsonString))
	ctx := context.WithValue(req.Context(), utils.FootPrintKey, "")
	req = req.WithContext(ctx)
	resp := httptest.NewRecorder()
	GeoCodeHandler(resp, req, httprouter.Params{})

	defer resp.Result().Body.Close()
	assert.Equal(t, resp.Result().StatusCode, http.StatusNotFound, "It should be equal")
}

type MockGoogleMapServiceDown struct{}

func (g *MockGoogleMapServiceDown) Get(address, region string) (*types.GeoCodeResponse, error) {
	return nil, errors.New("i am an error")
}

func TestFailGeoCodeHandlerByGoogleMapServiceDown(t *testing.T) {
	gmap.MapService = &MockGoogleMapServiceDown{}
	jsonString := `
		{
			"address": "CUHK",
			"region": "HK"
		}
	`
	req := httptest.NewRequest("POST", "http://127.0.0.1:3000/GeoCode", strings.NewReader(jsonString))
	ctx := context.WithValue(req.Context(), utils.FootPrintKey, "")
	req = req.WithContext(ctx)
	resp := httptest.NewRecorder()
	GeoCodeHandler(resp, req, httprouter.Params{})

	defer resp.Result().Body.Close()
	assert.Equal(t, resp.Result().StatusCode, http.StatusInternalServerError, "It should be equal")
}

func TestPassRouteNotFound(t *testing.T) {
	req := httptest.NewRequest("POST", "http://127.0.0.1:3000/foo", nil)
	ctx := context.WithValue(req.Context(), utils.FootPrintKey, "")
	req = req.WithContext(ctx)
	resp := httptest.NewRecorder()
	RouteNotFoundHandler(resp, req)

	responseData, _ := ioutil.ReadAll(resp.Body)
	responseString := string(responseData)
	defer resp.Result().Body.Close()
	assert.Equal(t, resp.Result().StatusCode, http.StatusNotFound, "It should be equal")
	assert.Equal(t, fmt.Sprintf("error: %s, footPrint:%s \n", utils.RouteNotFound, ""), responseString)
}
