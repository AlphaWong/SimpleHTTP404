package routes

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/SimpleHTTP404/gmap"
	"gitlab.com/SimpleHTTP404/middleware"
	"gitlab.com/SimpleHTTP404/types"
	"gitlab.com/SimpleHTTP404/utils"

	"github.com/julienschmidt/httprouter"
)

func GeoCodeHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var req types.GeoCodeRequest
	footPrint := r.Context().Value(utils.FootPrintKey).(string)

	if err := ParseRequest(r, &req); nil != err {
		log.Printf("footPrint: %s error: %v", footPrint, err)
		http.Error(w, fmt.Sprintf("error: %v, footPrint: %v", err.Error(), footPrint), http.StatusBadRequest)
		return
	}

	response, err := gmap.MapService.Get(req.Address, req.Region)
	if nil != err {
		log.Printf("footPrint: %s error: %v", footPrint, err)
		http.Error(w, fmt.Sprintf("error: %v, footPrint: %v", err.Error(), footPrint), http.StatusInternalServerError)
		return
	}
	response.FootPrint = footPrint
	if err := IsValidGeoResponse(response); nil != err {
		log.Printf("footPrint: %s error: %v", footPrint, err)
		http.Error(w, fmt.Sprintf("error: %v, footPrint: %v", err.Error(), footPrint), http.StatusNotFound)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func IsValidGeoResponse(gcr *types.GeoCodeResponse) error {
	if gcr.Lat == "" || gcr.Lng == "" {
		return errors.New(utils.ZeroResult)
	}
	return nil
}

func ParseRequest(r *http.Request, t interface{}) error {
	defer r.Body.Close()
	return json.NewDecoder(r.Body).Decode(&t)
}

func RouteNotFoundHandler(w http.ResponseWriter, r *http.Request) {
	footPrint := r.Context().Value(utils.FootPrintKey).(string)
	http.Error(w, fmt.Sprintf("error: %v, footPrint: %v", utils.RouteNotFound, footPrint), http.StatusNotFound)
}

func NewRouter() http.Handler {
	router := httprouter.New()
	router.POST("/GeoCode", GeoCodeHandler)
	router.NotFound = RouteNotFoundHandler
	r := middleware.LoggingMiddleware(router)
	return r
}
