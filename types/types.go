package types

type (
	GeoCodeResponse struct {
		Lat       string `json:"lat"`
		Lng       string `json:"lng"`
		FootPrint string `json:"footPrint"`
	}

	GeoCodeRequest struct {
		Address string `json:"address"`
		Region  string `json:"region"`
	}
)
