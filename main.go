package main

import (
	"flag"
	"log"
	"net/http"

	"gitlab.com/SimpleHTTP404/gmap"
	"gitlab.com/SimpleHTTP404/routes"
)

const PORT = ":3000"

var googleMapApiKey = flag.String("key", "", "API key of Google Map")

func initEnv() {
	flag.Parse()
}

func main() {
	initEnv()
	gmap.AddMapService(*googleMapApiKey)
	route := routes.NewRouter()
	log.Printf("Server on %s\n", PORT)
	log.Fatal(http.ListenAndServe(PORT, route))
}
