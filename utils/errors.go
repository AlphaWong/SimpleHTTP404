package utils

const (
	RouteNotFound = "ROUTE_NOT_FOUND"
	ZeroResult    = "ZERO_RESULT"
)
