[![pipeline status](https://gitlab.com/AlphaWong/SimpleHTTP404/badges/master/pipeline.svg)](https://gitlab.com/AlphaWong/SimpleHTTP404/commits/master)
[![coverage](https://codecov.io/gl/AlphaWong/SimpleHTTP404/branch/master/graph/badge.svg)](https://codecov.io/gl/AlphaWong/SimpleHTTP404)

# Objective
Convert Address to GPS location ( GeoCode ) simple http service

# Requirement
```
go get -u github.com/golang/dep/cmd/dep
dep ensure -vender-only
```

# Test
```sh
go test ./...
```

# Postman collection
```
https://www.getpostman.com/collections/a0ea5b7fb99b55bc903f
```

# Run
```sh
// Default PORT is 3000
go run ./main.go -key=<GOOGLE_MAP_API_KEY>
```
# Limition
It cannot handle rate limit.
If you are looking for a rate limit feature. Please use Google Map Golang sdk https://github.com/googlemaps/google-maps-services-go after commit (1b92133)

# Report issue
Please open an issue
